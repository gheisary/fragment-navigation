package com.adp.fragmentnavigation.business.devel;


import com.adp.fragmentnavigation.BuildConfig;

/**
 * Provides utility methods for developers
 * Created by becze on 12/15/2015.
 */
public class DevelManager {
    public String getBuildDescription() {
        String buildString = "Build: debug " + BuildConfig.FLAVOR + " ";
//        buildString += BuildConfig.HAS_BUILD_NUMBER ? "#" + BuildConfig.BUILD_NUMBER : " by " + BuildConfig.USERNAME + "@" + BuildConfig.COMPUTERNAME;
//        buildString += " (" + BuildConfig.BUILD_TIME + ")";
        return buildString;
    }
}
