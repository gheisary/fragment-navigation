package com.adp.fragmentnavigation.business.dagger.modules;

import com.adp.fragmentnavigation.ui.drawer.DrawerManager;
import com.adp.fragmentnavigation.ui.navigation.NavigationManager;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Provides instances for navigation
 * Created by becze on 11/13/2015.
 */
@Module
public class NavigationModule {


    @Provides
    @Singleton
    protected NavigationManager provideNavigationManager() {
        return new NavigationManager();
    }

    @Provides
    @Singleton
    protected DrawerManager provideDrawerManager() {
        return new DrawerManager();
    }
}
