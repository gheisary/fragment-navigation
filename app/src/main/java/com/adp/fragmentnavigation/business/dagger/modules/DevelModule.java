package com.adp.fragmentnavigation.business.dagger.modules;

import com.adp.fragmentnavigation.business.devel.DevelManager;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Provides developer helper modules
 * Created by becze on 12/15/2015.
 */
@Module
public class DevelModule {

    @Provides
    @Singleton
    protected DevelManager providesDevelManager() {
        return new DevelManager();
    }
}
