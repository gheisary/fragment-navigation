package com.adp.fragmentnavigation.business.dagger;

import android.app.Application;
import com.adp.fragmentnavigation.business.dagger.modules.AppModule;
import com.adp.fragmentnavigation.business.dagger.modules.DevelModule;
import com.adp.fragmentnavigation.business.dagger.modules.NavigationModule;
import dagger.Component;

import javax.inject.Singleton;

/**
 * Dagger interface which connects all the modules
 * Created by becze on 9/17/2015.
 */
@Singleton
@Component(modules = { AppModule.class, NavigationModule.class, DevelModule.class})
public interface MainComponent extends DaggerComponentGraph {

    final class Initializer {

        public static MainComponent init(Application app) {

            //@formatter:off
            return DaggerMainComponent.builder()
                            .appModule(new AppModule(app))
                            .navigationModule(new NavigationModule())
                            .develModule(new DevelModule())
                            .build();
            //@formatter:on
        }

    }
}
