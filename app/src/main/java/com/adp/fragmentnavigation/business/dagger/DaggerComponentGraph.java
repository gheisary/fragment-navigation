package com.adp.fragmentnavigation.business.dagger;


import com.adp.fragmentnavigation.FragmentNavigationDemoApplication;
import com.adp.fragmentnavigation.ui.BaseActivity;
import com.adp.fragmentnavigation.ui.MainActivity;
import com.adp.fragmentnavigation.ui.SampleFragment;
import com.adp.fragmentnavigation.ui.drawer.DrawerManager;
import com.adp.fragmentnavigation.ui.module1.Module1Fragment;
import com.adp.fragmentnavigation.ui.module1.Module1InternalFragment;
import com.adp.fragmentnavigation.ui.module2.Module2Fragment;
import com.adp.fragmentnavigation.ui.module3.Module3Fragment;
import com.adp.fragmentnavigation.ui.module4.Module4Fragment;
import com.adp.fragmentnavigation.ui.module5.Module5Fragment;

/**
 * Here are listed all the loations where injection is needed.
 * Created by becze on 9/17/2015.
 */
public interface DaggerComponentGraph {

    void inject(FragmentNavigationDemoApplication app);

    void inject(BaseActivity baseActivity);

    void inject(SampleFragment sampleFragment);

    void inject(Module3Fragment rulesOverviewFragment);

    void inject(MainActivity baseActivity);

    void inject(DrawerManager drawerManager);

    void inject(Module2Fragment conditionsOverviewFragment);

    void inject(Module5Fragment eventsOverviewFragment);

    void inject(Module1Fragment actionsOverviewFragment);

    void inject(Module4Fragment historyListFragment);

    void inject(Module1InternalFragment module1InternalFragment);
}
