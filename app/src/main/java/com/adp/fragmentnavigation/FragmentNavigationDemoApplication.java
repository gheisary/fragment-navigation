package com.adp.fragmentnavigation;

import android.app.Application;
import android.graphics.Typeface;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;
import com.facebook.stetho.Stetho;
import com.norbsoft.typefacehelper.TypefaceCollection;
import com.norbsoft.typefacehelper.TypefaceHelper;

public class FragmentNavigationDemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initLibs();
    }

    private void initLibs() {

        // Initialize dagger
        DaggerManager.getInstance().buildComponentAndInject(this);

        //@formatter:off
        // Initialize typeface helper
        TypefaceCollection typeface = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf"))
                .set(Typeface.BOLD, Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf"))
                .create();
        TypefaceHelper.init(typeface);
        //@formatter:on

        if (BuildConfig.DEBUG) {
            // Facebook Stetho
            Stetho.initializeWithDefaults(this);
        }
    }
}
