package com.adp.fragmentnavigation.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.adp.fragmentnavigation.R;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;

/**
 * [TODO]
 * Created by becze on 11/25/2015.
 */
public class SampleFragment extends BaseFragment {

    private static final String TAG = SampleFragment.class.getSimpleName();

    public static SampleFragment newInstance() {
        return new SampleFragment();
    }
    private View mRootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerManager.component().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_sample_layout, null);
        }
        return mRootView;
    }
}
