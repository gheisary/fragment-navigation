package com.adp.fragmentnavigation.ui.module1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.adp.fragmentnavigation.R;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;
import com.adp.fragmentnavigation.ui.BaseFragment;

/**
 * Example Fragment
 * Created by becze on 11/25/2015.
 */
public class Module1InternalFragment extends BaseFragment {


    private static final String TAG = Module1InternalFragment.class.getSimpleName();
    public static Module1InternalFragment newInstance() {
        return new Module1InternalFragment();
    }
    private View mRootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerManager.component().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.internal_layout, null);
        }
        return mRootView;
    }
}
