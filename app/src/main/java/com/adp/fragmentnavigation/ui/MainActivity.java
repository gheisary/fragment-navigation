package com.adp.fragmentnavigation.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import com.adp.fragmentnavigation.R;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;
import com.adp.fragmentnavigation.ui.drawer.DrawerManager;
import com.adp.fragmentnavigation.ui.navigation.NavigationManager;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import javax.inject.Inject;

/**
 * Main activity which holds the container for the fragment navigation.
 */
public class MainActivity extends BaseActivity implements Drawer.OnDrawerItemClickListener, NavigationManager.NavigationListener {

    @Inject
    public NavigationManager mNavigationManager;

    @Inject
    public DrawerManager mDrawerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inject members
        DaggerManager.component().inject(this);

        // Initialize the NavigationManager with this activity's FragmentManager
        mNavigationManager.init(getSupportFragmentManager());
        mNavigationManager.setNavigationListener(this);

        // start as the first screen the rules overview
        mNavigationManager.startMenu1();

        initDrawer();
    }

    public void initDrawer() {
        mDrawerManager.buildDrawer(this, mToolbar);
        mDrawerManager.getDrawer().setOnDrawerItemClickListener(this);
    }

    @Override
    public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {
        boolean isHandled = mDrawerManager.handleItemSelected(this, view, i, iDrawerItem);
        if (isHandled) {
            mDrawerManager.getDrawer().closeDrawer();
        }

        return isHandled;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            // we have only one fragment left so we would close the application with this back
            showExitDialog();
        } else {
            mNavigationManager.navigateBack(this);
        }
    }

    protected void showExitDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.exit_message).setCancelable(false).setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        }).setNegativeButton(android.R.string.cancel, null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onBackstackChanged() {
        // check if we display a root fragment and enable drawer only on root fragments
        boolean rootFragment = mNavigationManager.isRootFragmentVisible();
        mDrawerManager.enableDrawer(rootFragment);
        mDrawerManager.enableActionBarDrawerToggle(rootFragment);
    }
}
