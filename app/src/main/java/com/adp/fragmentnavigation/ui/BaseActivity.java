package com.adp.fragmentnavigation.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import butterknife.ButterKnife;
import com.adp.fragmentnavigation.R;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;
import com.adp.fragmentnavigation.ui.navigation.NavigationManager;
import com.norbsoft.typefacehelper.TypefaceHelper;

import javax.inject.Inject;

/**
 * Base activity for all the Activities, it provides some common operation for all of the sub-activities.
 * Created by becze on 9/21/2015.
 */
public class BaseActivity extends AppCompatActivity {

    public Toolbar mToolbar;

    public FrameLayout mContentLayout;

    @Inject
    public NavigationManager mNavigationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        DaggerManager.component().inject(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        // We set a toolbar layout as the default and inflate into the content layout
        super.setContentView(R.layout.toolbar_layout);
        mToolbar = findViewById(R.id.toolbar);
        mContentLayout = findViewById(R.id.content);

        // use the new toolbar
        setSupportActionBar(mToolbar);

        // Get an inflater
        getLayoutInflater().inflate(layoutResID, mContentLayout);
        ButterKnife.bind(this);
        TypefaceHelper.typeface(this);
    }

    public void setToolbarVisibility(int visibility) {
        mToolbar.setVisibility(visibility);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mNavigationManager.navigateBack(this);
    }
}
