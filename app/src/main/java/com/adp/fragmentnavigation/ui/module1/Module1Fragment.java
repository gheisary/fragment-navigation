package com.adp.fragmentnavigation.ui.module1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.OnClick;
import com.adp.fragmentnavigation.R;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;
import com.adp.fragmentnavigation.ui.BaseFragment;
import com.adp.fragmentnavigation.ui.navigation.NavigationManager;

import javax.inject.Inject;

/**
 * Example Fragment
 * Created by becze on 11/25/2015.
 */
public class Module1Fragment extends BaseFragment {

    private static final String TAG = Module1Fragment.class.getSimpleName();

    public static Module1Fragment newInstance() {
        return new Module1Fragment();
    }
    private View mRootView;

    @Inject
    NavigationManager mNavigationManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerManager.component().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.module1_layout, null);
        }
        return mRootView;
    }

    @OnClick(R.id.internal_module_button)
    void navigate() {
        mNavigationManager.startInternalModule();
    }
}
