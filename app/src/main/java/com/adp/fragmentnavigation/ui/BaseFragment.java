package com.adp.fragmentnavigation.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import butterknife.ButterKnife;
import com.adp.fragmentnavigation.R;
import com.norbsoft.typefacehelper.TypefaceHelper;

/**
 * Base class for every Fragment, it provides some custom behavior for all of the fragments
 * sucn as dependency injection.
 * Created by becze on 9/21/2015.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        TypefaceHelper.typeface(view);

        initActionBar(true, getString(R.string.app_name));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w("BSZ", "onDestroy: " + this);
    }

    protected void initActionBar(boolean showHomeButton, String title) {
        if (getActivity() != null && getActivity() instanceof BaseActivity ) {
            ActionBar supportActionBar = ((BaseActivity) getActivity()).getSupportActionBar();
            supportActionBar.setHomeButtonEnabled(showHomeButton);
            supportActionBar.setTitle(title);
        }
    }

    protected void showActionbar(boolean isShown) {
        if (getActivity() != null && getActivity() instanceof BaseActivity ) {
            ActionBar supportActionBar = ((BaseActivity) getActivity()).getSupportActionBar();
            if (isShown) {
                supportActionBar.show();
            } else {
                supportActionBar.hide();
            }
        }
    }
}
