package com.adp.fragmentnavigation.ui.module5;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.adp.fragmentnavigation.R;
import com.adp.fragmentnavigation.business.dagger.DaggerManager;
import com.adp.fragmentnavigation.ui.BaseFragment;

/**
 * Example fragment
 * Created by becze on 11/25/2015.
 */
public class Module5Fragment extends BaseFragment {

    private static final String TAG = Module5Fragment.class.getSimpleName();
    public static Module5Fragment newInstance() {
        return new Module5Fragment();
    }

    private View mRootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerManager.component().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.module5_layout, null);
        }
        return mRootView;
    }
}
